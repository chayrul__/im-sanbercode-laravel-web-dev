<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class WelcomeController extends Controller
{
    

    public function greeting(Request $request){
        $firstName = $request['namaDepan'];
        $lastName = $request['namaBelakang'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $bahasa = $request['language'];
        return view('welcome',['namaDepan' => $firstName, 'namaBelakang'=>$lastName, 'gender' =>$gender, 'nationality'=>$nationality, 'language'=>$bahasa]);
    }


}
