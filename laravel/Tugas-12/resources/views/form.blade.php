

<!DOCTYPE html>
<html lang="en">

<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/submitNama" method="POST">
        @csrf
        <label>First Name: </label><br>
        <input type="text" name="namaDepan" id="firstName"><br>
        <label>Last Name:</label><br>
        <input type="text" name="namaBelakang" id="lastName"><br><br>
        <label for="gender">Gender: </label><br><br>
        <input type="radio" name="gender" id="male" value="0">Male <br>
        <input type="radio" name="gender" id="female" value="1">Female <br>
        <input type="radio" name="gender" value="3">Other <br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singapura">Singapura</option>
            <option value="malaysia">Malaysia</option>
            <option value="australia">Australia</option>
        </select><br><br>
        <label for="language">Language Spoken: </label><br>
        <input type="checkbox" name="language" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="2">English <br>
        <input type="checkbox" name="language" value="3">Other <br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Submit">
    </form>
</body>

</html>