@extends('layout.master')
@section('judul')
    Detail Caster
@endsection
@section('subJudul')
    Detail Caster {{$cast->nama}}
@endsection
@section('content')
    <h4>Nama: {{$cast->nama}}</h4>
    <h4>Umur: {{$cast->umur}}</h4>
    <h4>Bio: {{$cast->bio}}</h4>
    <a href="/cast" class="btn btn-outline-secondary">Kembali</a>
    @endsection