@extends('layout.master')
@section('judul')
    Tambah Caster
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control"  name="nama" placeholder="Masukkan Nama">
      @error('nama')
        <div class="alert alert-danger">
            {{$message}}
        </div>
      @enderror
    </div>
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="number" class="form-control" name="umur">
      @error('umur')
        <div class="alert alert-danger">
            {{$message}}
        </div>
      @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" rows="3"></textarea>
        @error('bio')
        <div class="alert alert-danger ">
            {{$message}}
        </div>
      @enderror
      </div>
    <button type="submit" class="btn btn-outline-primary">Submit</button>
    <a href="/cast" class="btn btn-outline-secondary">Ke Halaman Daftar Cast</a>
  </form>
@endsection