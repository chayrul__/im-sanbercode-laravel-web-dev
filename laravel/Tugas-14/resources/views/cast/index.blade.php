@extends('layout.master')
@section('judul')
    Daftar Caster
@endsection
@section('subJudul')
    <a href="/cast/create" class="btn btn-primary btn-sm">Tambah Caster</a>
@endsection

@section('content')
<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>{{$value->bio}}</td>
            <td>
                <form action="/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$value->id}}" class="btn btn-outline-info">Show</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-outline-success">Edit</a>
                    <input type="submit" class="btn btn-outline-danger my-1" value="Hapus">
                </form>
            
            </td>
            
        </tr>
        @empty
        <tr>
            <td>Tidak ada data</td>
        </tr>
        @endforelse
    </tbody>
  </table>
@endsection