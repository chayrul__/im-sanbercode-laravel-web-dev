<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Routing Tugas 15
// Show caster
Route::get('/cast',[CastController::class,'index']);
// add caster
Route::get('cast/create',[CastController::class,'create']);
Route::post('/cast',[CastController::class,'store']);

// show data caster
Route::get('/cast/{cast_id}',[CastController::class,'show']);

// Edit data caster
Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']);
Route::put('/cast/{cast_id}',[CastController::class,'update']);
// Hapus data caster
Route::delete('/cast/{cast_id}',[CastController::class,'destroy']);