<?php

require ('Frog.php');
require ('Ape.php');

$sheep = new Animal("shaun");
echo "Name: ".$sheep->name. "<br>"; // "shaun"
echo "Legs: ".$sheep->get_legs()."<br>"; // 4
echo "Cold Blooded: ".$sheep->get_cold_blooded()."<br><br>"; // "no"

$sungokong = new Ape("kera sakti");
echo "Name: ".$sungokong->name. "<br>"; // "shaun"
echo "Legs: ".$sungokong->get_legs()."<br>"; // 4
echo "Cold Blooded: ".$sungokong->get_cold_blooded()."<br>";
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "Name: ".$kodok->name. "<br>";
echo "Legs: ".$kodok->get_legs()."<br>"; // 4
echo "Cold Blooded: ".$kodok->get_cold_blooded()."<br>";
$kodok->jump(); // "hop hop"

?>